import os,sys,glob,time,collections,gc,pickle
import numpy as np
from netCDF4 import Dataset,num2date
import matplotlib.pylab as plt
import pandas as pd

sys.path.append('/Users/peterpfleiderer/Projects/git-packages/wlcalculator/app/')
import wacalc.CmipData as CmipData; reload(CmipData)

warming_levels = pd.DataFrame()
warming_levels['wlvl'] = np.arange(1,2.5,0.5)

for model in {'IPSL':'ipsl-cm5a-lr','HADGEM2':'hadgem2-es','ECEARTH':'ec-earth','MPIESM':'mpi-esm-lr'}.values():
	run = 'r1i1p1'
	cmipdata = CmipData.CmipData('CMIP5',[model],['rcp45','historical'])
	cmipdata.get_cmip(runs = [run])
	cmipdata.compute_period( [1986,2006], [1850,1900], warming_levels['wlvl'], window=21)
	lvls=cmipdata.exceedance_tm
	for scenario in lvls.scenario:
		warming_levels[model+'_'+scenario] = np.array(lvls[scenario,:].values, np.int)

warming_levels.to_csv('/Users/peterpfleiderer/Projects/online_tools/regioClim_Ghana/small_data/GHA/warming_levels.csv')
#
