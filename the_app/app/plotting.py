# -*- coding: utf-8 -*-

# Copyright (C) 2017 Peter Pfleiderer
#
# This file is part of regioclim.
#
# regioclim is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# regioclim is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License
# along with regioclim; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


import os,sys,importlib,time,psutil,pickle,json

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy
import numpy as np
import xarray as xr
import numpy as np

from app import settings

from app import class_on_shape

'''

	session = {}
	session['country']     = 'GHA'
	session['region']     = 'GHA'
	session["indicator"]     = 'TXx'
	session["season"]     = 'annual'

	session['use_periods'] = 1
	session["period_ref"]     = [1986,2006]
	session["period_proj"]    = [2031,2051]
	session['wlvl_ref'] = '0.61'
	session['wlvl'] = '2.0'



	COU=class_on_shape.shaped_object(iso=session['country'], working_directory='../small_data/'+session['country'])
	COU.load_shapefile()

'''


def rearange_grid(to_plot):
	x,y=to_plot.lon.values.copy(),to_plot.lat.values.copy()
	x-=np.diff(x,1)[0]/2.
	y-=np.diff(y,1)[0]/2.
	x=np.append(x,[x[-1]+np.diff(x,1)[0]])
	y=np.append(y,[y[-1]+np.diff(y,1)[0]])
	x,y=np.meshgrid(x,y)
	return x,y

def plot_reference_map(session,COU,mask,out_format='_small.png'):
	start_time = time.time()

	plot_file_name='static/COU_images/'+session['country']+'/'+'_'.join(['refMap',session['indicator'],session['season'],session['ref_id']])+out_format

	if os.path.isfile('app/'+plot_file_name) == False:
		tmp = COU.select_griddedData('monthly',tags={'source':'EWEMBI','var_name':[session['indicator']],'rcp':['historical'],'model':['EWEMBI']}).transpose("time", ...).squeeze().transpose("time", ...)
		tmp = tmp[np.isfinite(np.nanmean(tmp, axis=tuple(range(1,len(tmp.shape)))))]

		# find relevant months for the selected season
		relevant_months = np.zeros(tmp.time.shape[0], np.bool)
		for mon in settings.season_dict[session['season']]['months']:
			relevant_months[tmp.time.dt.month.values == mon] = True

		# compute seasonal mean
		tmp = tmp.squeeze()[relevant_months]
		tmp_seasonal = tmp.groupby('time.year').mean('time')

		to_plot = tmp_seasonal.loc[session['period_ref'][0]:session['period_ref'][1]].mean('year')
		to_plot.values[np.isnan(mask.values)] = np.nan

		print('prep',time.time() - start_time); start_time = time.time()

		color_range=np.nanpercentile(to_plot,[10,90])
		if np.mean(color_range)==0:		cmap = settings.indicator_dict[session['indicator']]['cmap']['div']
		elif np.mean(color_range)<0:	cmap = settings.indicator_dict[session['indicator']]['cmap']['neg']
		elif np.mean(color_range)>0:	cmap = settings.indicator_dict[session['indicator']]['cmap']['pos']

		x,y = rearange_grid(to_plot)

		plt.close('all')
		fig,ax = plt.subplots(nrows=1, figsize=(4,4*(len(y)/len(x))**0.5),subplot_kw={'projection': ccrs.PlateCarree()})
		ax.coastlines(resolution='10m');
		im = ax.pcolormesh(x,y,to_plot, cmap=cmap, vmin=color_range[0], vmax=color_range[1], transform=ccrs.PlateCarree())

		for shape in COU._region_polygons.values():
			ax.add_geometries([shape], ccrs.PlateCarree(), edgecolor='k',alpha=1,facecolor='none',linewidth=0.5,zorder=50)
		cb = plt.colorbar(im, ax=ax)
		cb.set_label(settings.indicator_dict[session['indicator']]['name']+' ['+settings.indicator_dict[session['indicator']]['unit']+']', rotation=90)
		ax.set_title(u''+session['ref_name']+' '+' '.join([settings.season_dict[session['season']]['name'].replace('*','')]),fontsize=12)
		plt.savefig('test.png', bbox_inches='tight')
		
		if out_format=='_small.png':plt.savefig('app/'+plot_file_name, bbox_inches='tight')
		if out_format=='_large.png':plt.savefig('app/'+plot_file_name, bbox_inches='tight',dpi=300)
		if out_format=='.pdf':plt.savefig('app/'+plot_file_name, bbox_inches='tight', format='pdf', dpi=1000)
		print('plot',time.time() - start_time); start_time = time.time()
	return plot_file_name

def plot_projection_diff_map(session,COU,mask,out_format='_small.png'):
	start_time = time.time()

	plot_file_name='static/COU_images/'+session['country']+'/'+'_'.join(['projMap',session['indicator'],session['season'],session['ref_id'],session['proj_id']])+out_format
	if os.path.isfile('app/'+plot_file_name) == False:# or True:
		proj = COU.select_griddedData('monthly',tags={'rcp':'rcp45','source':['CORDEX_BC'],'model':['ECEARTH', 'HADGEM2', 'IPSL', 'MPIESM']}).transpose("time", ...)
		proj = proj[np.isfinite(np.nanmean(proj, axis=tuple(range(1,len(proj.shape)))))]
		ref = COU.select_griddedData('monthly',tags={'rcp':'historical','source':['CORDEX_BC'],'model':['ECEARTH', 'HADGEM2', 'IPSL', 'MPIESM']}).transpose("time", ...)
		ref = ref[np.isfinite(np.nanmean(ref, axis=tuple(range(1,len(ref.shape)))))]
		tmp = xr.concat((ref,proj), dim='time').squeeze()

		# find relevant months for the selected season
		relevant_months = np.zeros(tmp.time.shape[0], np.bool)
		for mon in settings.season_dict[session['season']]['months']:
			relevant_months[tmp.time.dt.month.values == mon] = True

		# compute seasonal mean
		tmp = tmp.squeeze()[relevant_months]
		if session['indicator'] in ['pr']:
			tmp_seasonal = tmp.groupby('time.year').sum('time')
		else:
			tmp_seasonal = tmp.groupby('time.year').mean('time')


		diff = tmp_seasonal[0].copy() * np.nan
		for model in diff.model.values:
			diff.loc[model] = tmp_seasonal.loc[session['years_proj'][model],model].mean('year') - tmp_seasonal.loc[session['years_ref'][model],model].mean('year')

		mean_diff = diff.median('model')
		mean_diff.values[np.isnan(mask.values)] = np.nan

		agree = mean_diff.copy()*0
		for model in diff.model.values:
			agree += np.sign(mean_diff) == np.sign(diff.loc[model])

		agree.values[agree.values>2] = np.nan
		agree.values[agree.values<3] = 0.5
		print('prep',time.time() - start_time); start_time = time.time()

		print(mean_diff)
		color_range = np.nanpercentile(mean_diff,[10,90])
		print(color_range)
		if np.sign(color_range[0]) != np.sign(color_range[1]):
			color_range=[-np.max(np.abs(color_range)),np.max(np.abs(color_range))]
			cmap = settings.indicator_dict[session['indicator']]['cmap']['div']
		elif np.mean(color_range)<0:
			cmap = settings.indicator_dict[session['indicator']]['cmap']['neg']
		elif np.mean(color_range)>0:
			cmap = settings.indicator_dict[session['indicator']]['cmap']['pos']

		x,y = rearange_grid(mean_diff)

		plt.close('all')
		fig,ax = plt.subplots(nrows=1, figsize=(4,4*(len(y)/len(x))**0.5),subplot_kw={'projection': ccrs.PlateCarree()})
		ax.coastlines(resolution='10m');
		im = ax.pcolormesh(x,y,mean_diff, cmap=cmap, vmin=color_range[0], vmax=color_range[1], transform=ccrs.PlateCarree())
		ax.pcolormesh(x,y,agree, cmap='Greys', vmin=0, vmax=1, transform=ccrs.PlateCarree())

		for shape in COU._region_polygons.values():
			ax.add_geometries([shape], ccrs.PlateCarree(), edgecolor='k',alpha=1,facecolor='none',linewidth=0.5,zorder=50)
		cb = plt.colorbar(im, ax=ax)
		cb.set_label(settings.indicator_dict[session['indicator']]['name']+' ['+settings.indicator_dict[session['indicator']]['unit']+']', rotation=90)
		ax.set_title(u''+session['proj_name']+' vs '+session['ref_name']+' '+' '.join([settings.season_dict[session['season']]['name'].replace('*','')]),fontsize=12)

		if out_format=='_small.png':plt.savefig('app/'+plot_file_name, bbox_inches='tight')
		if out_format=='_large.png':plt.savefig('app/'+plot_file_name, bbox_inches='tight',dpi=300)
		if out_format=='.pdf':plt.savefig('app/'+plot_file_name, bbox_inches='tight', format='pdf', dpi=1000)
		print('plot',time.time() - start_time); start_time = time.time()
	return plot_file_name

def plot_transient(session,COU,mask,out_format='_small.png'):
	start_time = time.time()

	plot_file_name='static/COU_images/'+session['country']+'/'+'_'.join(['trans',session['indicator'],session['season'],session['region']])+out_format

	if os.path.isfile('app/'+plot_file_name) == False:
		print('read',time.time() - start_time); start_time = time.time()
		proj = COU.select_griddedData('monthly',tags={'rcp':'rcp45','source':['CORDEX_BC'],'model':['ECEARTH', 'HADGEM2', 'IPSL', 'MPIESM']}).transpose("time", ...)
		proj = proj[np.isfinite(np.nanmean(proj, axis=tuple(range(1,len(proj.shape)))))]
		ref = COU.select_griddedData('monthly',tags={'rcp':'historical','source':['CORDEX_BC'],'model':['ECEARTH', 'HADGEM2', 'IPSL', 'MPIESM']}).transpose("time", ...)
		ref = ref[np.isfinite(np.nanmean(ref, axis=tuple(range(1,len(ref.shape)))))]
		tmp = xr.concat((ref,proj), dim='time').squeeze()

		# find relevant months for the selected season
		relevant_months = np.zeros(tmp.time.shape[0], np.bool)
		for mon in settings.season_dict[session['season']]['months']:
			relevant_months[tmp.time.dt.month.values == mon] = True

		# compute seasonal mean
		tmp = tmp.squeeze()[relevant_months]
		tmp_seasonal = tmp.groupby('time.year').mean('time')

		tmp = (tmp_seasonal * mask).sum('lat').sum('lon').loc[:2099]

		ens = tmp.copy()
		for model in tmp.model.values:
			ens.loc[:,model] = tmp.loc[:,model].rolling(year=20, center=True).mean().values

		plt.close('all')
		fig,ax = plt.subplots(nrows=1, figsize=(4,4))

		ax.fill_between(ens.year, ens.min('model'), ens.max('model'), color='green', alpha=0.2)
		ax.plot(ens.year, ens.mean('model'), color='green')
		ax.set_ylabel(settings.indicator_dict[session['indicator']]['name']+' ['+settings.indicator_dict[session['indicator']]['unit']+']', rotation=90)
		ax.set_title(u''+' '.join([settings.season_dict[session['season']]['name'].replace('*',''), COU._region_names[session['region']].replace('_',' ')]),fontsize=12)

		if out_format=='_small.png':plt.savefig('app/'+plot_file_name, bbox_inches='tight')
		if out_format=='_large.png':plt.savefig('app/'+plot_file_name, bbox_inches='tight',dpi=300)
		if out_format=='.pdf':plt.savefig('app/'+plot_file_name, bbox_inches='tight', format='pdf', dpi=1000)
		print('plot',time.time() - start_time); start_time = time.time()
	return plot_file_name

def plot_annual_cycle(session,COU,mask,out_format='_small.png'):
	start_time = time.time()

	plot_file_name='static/COU_images/'+session['country']+'/'+'_'.join(['annualCyc',session['indicator'],session['region'],session['ref_id'],session['proj_id']])+out_format

	if os.path.isfile('app/'+plot_file_name) == False:# or True:

		print('read',time.time() - start_time); start_time = time.time()
		proj = COU.select_griddedData('monthly',tags={'rcp':'rcp45','source':['CORDEX_BC'],'model':['ECEARTH', 'HADGEM2', 'IPSL', 'MPIESM']}).transpose("time", ...)
		proj = proj[np.isfinite(np.nanmean(proj, axis=tuple(range(1,len(proj.shape)))))]
		ref = COU.select_griddedData('monthly',tags={'rcp':'historical','source':['CORDEX_BC'],'model':['ECEARTH', 'HADGEM2', 'IPSL', 'MPIESM']}).transpose("time", ...)
		ref = ref[np.isfinite(np.nanmean(ref, axis=tuple(range(1,len(ref.shape)))))]
		merged = xr.concat((ref,proj), dim='time').squeeze()

		projs = []
		for model in proj.model.values:
			tmp = merged.sel(model=model).squeeze()
			tmp = tmp.loc[np.datetime64(str(session['years_proj'][model][0])+'-01-15'):np.datetime64(str(session['years_proj'][model][1])+'-12-31')]
			tmp = tmp.groupby('time.month').mean('time').squeeze()
			tmp = (tmp * mask).sum('lat').sum('lon')
			projs.append(tmp)
		proj = xr.concat(projs, dim='model')

		refs = []
		for model in ref.model.values:
			tmp = merged.sel(model=model).squeeze()
			tmp = tmp.loc[np.datetime64(str(session['years_ref'][model][0])+'-01-15'):np.datetime64(str(session['years_ref'][model][1])+'-12-31')]
			tmp = tmp.groupby('time.month').mean('time').squeeze()
			tmp = (tmp * mask).sum('lat').sum('lon')
			refs.append(tmp)
		ref = xr.concat(refs, dim='model')

		ewembi = COU.select_griddedData('monthly',tags={'rcp':'historical','source':['EWEMBI'],'model':['EWEMBI']}).transpose("time", ...)
		ewembi = ewembi[np.isfinite(np.nanmean(ewembi, axis=tuple(range(1,len(ewembi.shape)))))]
		ewembi = ewembi.loc[np.datetime64(str(session['period_ref'][0])+'-01-15'):np.datetime64(str(session['period_ref'][1])+'-12-31')]
		ewembi = ewembi.groupby('time.month').mean('time').squeeze()
		ewembi = (ewembi * mask).sum('lat').sum('lon')

		fig,axes=plt.subplots(nrows=2,ncols=1,sharex=True,figsize=(5,4))
		ax = axes[0]
		ax.plot(ewembi.month, ref.median('model'), color='green', label='model data')
		ax.fill_between(ewembi.month,ref.min('model'),ref.max('model'), alpha=0.2, color='green')
		ax.plot(ewembi.month, ewembi.values, color='k', label='observations (EWEMBI)')
		leg = axes[0].legend(loc='best',fancybox=True,fontsize=10)
		leg.get_frame().set_alpha(0.3)
		ax.set_title(u''+COU._region_names[session['region']].replace('_',' ')+' '+session['ref_name'] ,fontsize=10)

		ax = axes[1]
		ax.plot(ewembi.month, np.median(proj-ref,axis=0), label='projected change', color='green')
		ax.fill_between(ewembi.month,np.min(proj-ref,axis=0),np.max(proj-ref,axis=0), alpha=0.2, color='green')
		ax.axhline(y=0, color='k')
		leg = axes[1].legend(loc='best',fancybox=True,fontsize=10)
		leg.get_frame().set_alpha(0.3)
		ax.set_title(u''+session['proj_name']+' vs '+session['ref_name'],fontsize=10)

		ax.set_xticks(range(1,13))
		ax.set_xticklabels(['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'])
		plt.annotate(settings.indicator_dict[session['indicator']]['name']+' ['+settings.indicator_dict[session['indicator']]['unit']+']', xy=(0.01,0.5), xycoords='figure fraction', rotation=90, ha='left', va='center')
		if out_format=='_small.png':plt.savefig('app/'+plot_file_name, bbox_inches='tight')
		if out_format=='_large.png':plt.savefig('app/'+plot_file_name, bbox_inches='tight',dpi=300)
		if out_format=='.pdf':plt.savefig('app/'+plot_file_name, bbox_inches='tight', format='pdf', dpi=1000)

		print('plot',time.time() - start_time); start_time = time.time()
	return plot_file_name

def plot_location(session,COU,mask):
	plot_file_name = 'static/COU_images/'+session['country']+'/loc_'+session['region']+'.png'
	if os.path.isfile(plot_file_name)==False:
		COU.load_mask()
		mask = COU._masks[list(COU._masks.keys())[0]]['overlap']
		asp=(float(len(mask.lon))/float(len(mask.lat)))**0.5

		fig,ax=plt.subplots(nrows=1,ncols=1,figsize=(5,5),subplot_kw={'projection': ccrs.PlateCarree()})
		ax.coastlines(resolution='10m');
		im = ax.pcolormesh(mask.lon,mask.lat,mask[0], alpha=0, transform=ccrs.PlateCarree())
		ax.add_geometries([COU._region_polygons[session['country']]], ccrs.PlateCarree(), edgecolor='k',alpha=1,facecolor=[0,0,0.5],linewidth=0.5,zorder=1)
		ax.add_geometries([COU._region_polygons[session['region']]], ccrs.PlateCarree(), edgecolor='k',alpha=1,facecolor='orange',linewidth=0.5,zorder=2)
		ax.set_axis_off()
		ax.outline_patch.set(edgecolor='w')
		plt.subplots_adjust(left=0, right=1, top=1, bottom=0)

		plt.savefig('app/'+plot_file_name, bbox_inches='tight',dpi=300)

	return plot_file_name








#
