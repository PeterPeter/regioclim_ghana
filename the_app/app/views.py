# -*- coding: utf-8 -*-

# Copyright (C) 2017 Peter Pfleiderer
#
# This file is part of regioclim.
#
# regioclim is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# regioclim is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.    See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License
# along with regioclim; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA    02110-1301    USA

import os,glob,sys,time,random,pickle,string,gc,subprocess,importlib,gc
from app import app
from flask import redirect, render_template, url_for, request, flash, get_flashed_messages, g, session, jsonify, Flask, send_from_directory, request
import matplotlib.pylab as plt
from app.plotting import *


# import resource
# mem_usage = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss

from app import settings
from app import forms
from app import plotting

from app import class_on_shape

# this is the initial landing page
# here default choices are defined
@app.route('/')
def index():
	session['country']     = 'GHA'
	session['region']     = 'GHA'
	session['new_region_name'] = ''
	session['regions_to_merge'] = []
	session['new_region_name_auto'] = True
	session['small_region_warning'] = False

	session["indicator"]     = 'TXx'
	session["season"]     = 'annual'

	session['use_periods'] = 1
	session["period_ref"]     = [1986,2006]
	session["period_proj"]    = [2031,2051]
	session['wlvl_ref'] = '0.61'
	session['wlvl'] = '2.0'

	session['location']='index'
	return redirect(url_for("choices"))


# this is the main page
@app.route('/choices', methods=['GET', 'POST'])
def choices():
	print(session)

	# here the data for the country is loaded
	COU=class_on_shape.shaped_object(iso=session['country'], working_directory='../small_data/'+session['country'])
	# shapefiles are read to get the names of all possible subregions in the country
	COU.load_shapefile()

	form_region = forms.regionForm(request.form)
	regions_available = [session['region']] + [reg for reg in list(COU._region_names.keys()) if reg != session['region']]
	form_region.regions.choices = list(zip(regions_available,[COU._region_names[reg].replace('_',' ') for reg in regions_available]))

	form_indicator = forms.indicatorForm(request.form)
	indicators_available = [session['indicator']] + [ind for ind in list(settings.indicator_dict.keys()) if ind != session['indicator']]
	form_indicator.indicators.choices = list(zip(indicators_available,[settings.indicator_dict[ind]['name'] for ind in indicators_available]))

	form_season = forms.seasonForm(request.form)
	indoicators_available = [session['season']] + [sea for sea in settings.season_dict.keys() if sea != session['season']]
	form_season.seasons.choices = list(zip(indoicators_available,[settings.season_dict[sea]['name'] for sea in indoicators_available ]))

	context = {
		'form_region':form_region,
		'form_season':form_season,
		'form_indicator':form_indicator,
	}

	if session['use_periods']:
		context['form_period'] = forms.PeriodField(request.form)
		context['form_period'] = forms.PeriodField(request.form, period_proj=settings.period_name(session["period_proj"]), period_ref=settings.period_name(session["period_ref"]))

		session['ref_id'] = '%s-%s' %tuple(session["period_ref"])
		session['proj_id'] = '%s-%s' %tuple(session["period_proj"])
		session['ref_name'] = '%s-%s' %tuple(session["period_ref"])
		session['proj_name'] = '%s-%s' %tuple(session["period_proj"])
		session['ref_longname'] = 'period %s-%s' %tuple(session["period_ref"])
		session['proj_longname'] = '%s-%s' %tuple(session["period_proj"])
		session['years_ref'] = {model:list(range(session["period_ref"][0],session["period_ref"][1],1)) for model in settings.wlvls.columns[1:]}
		session['years_proj'] = {model:list(range(session["period_proj"][0],session["period_proj"][1],1)) for model in settings.wlvls.columns[1:]}

	else:
		context['form_warming_lvl'] = forms.warming_lvlForm(request.form)
		wlvl_available=[session['wlvl']]+[w for w in settings.wlvl_dict.keys() if w not in [session['wlvl'],session['wlvl_ref'],'0.61']]
		context['form_warming_lvl'].warming_lvls.choices = list(zip(wlvl_available,[settings.wlvl_dict[sea] for sea in wlvl_available]))

		context['form_warming_lvl_ref'] = forms.warming_lvl_refForm(request.form)
		wlvl_available=[session['wlvl_ref']]+[w for w in settings.wlvl_dict.keys() if w not in [session['wlvl_ref'],session['wlvl'],'2.0']]
		context['form_warming_lvl_ref'].warming_lvl_refs.choices = list(zip(wlvl_available,[settings.wlvl_dict[sea] for sea in wlvl_available]))

		session['ref_id'] = session['wlvl_ref'].replace('.','p')
		session['proj_id'] = session['wlvl'].replace('.','p')
		session['ref_name'] = settings.wlvl_dict[session['wlvl_ref']].replace(' above preindustrial','')
		session['proj_name'] = settings.wlvl_dict[session['wlvl']].replace(' above preindustrial','')
		session['ref_longname'] = settings.wlvl_dict[session['wlvl_ref']]
		session['proj_longname'] = settings.wlvl_dict[session['wlvl']]

		session['years_ref'] = {model:list(range(settings.wlvls.loc[(settings.wlvls.wlvl == session['wlvl_ref']),model].values[0]-10,settings.wlvls.loc[(settings.wlvls.wlvl == session['wlvl_ref']),model].values[0]+10,1)) for model in settings.wlvls.columns[1:]}
		session['years_proj'] = {model:list(range(settings.wlvls.loc[(settings.wlvls.wlvl == session['wlvl']),model].values[0]-10,settings.wlvls.loc[(settings.wlvls.wlvl == session['wlvl']),model].values[0]+10,1)) for model in settings.wlvls.columns[1:]}

	start_time = time.time()
	#print(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)
	COU.meta_load()
	COU.griddedData_read(tags={'var_name':[session['indicator']]})
	if session['indicator'] in ['tas','TXx']:
		COU.griddedData_unit_conversion(shift=-273.15)

	COU.load_mask()
	mask = COU._masks[list(COU._data.keys())[0]]['latWeight'].loc[session['region']]
	#print(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)
	print('loaded',time.time() - start_time); start_time = time.time()

	context['fl_plot_reference_map'] = plot_reference_map(session,COU,mask)
	print('plot_reference_map',time.time() - start_time); start_time = time.time()
	context['fl_plot_projection_diff_map'] = plot_projection_diff_map(session,COU,mask)
	print('plot_projection_diff_map',time.time() - start_time); start_time = time.time()
	context['fl_plot_transient'] = plot_transient(session,COU,mask)
	print('plot_transient',time.time() - start_time); start_time = time.time()
	context['fl_plot_annual_cycle'] = plot_annual_cycle(session,COU,mask)
	print('plot_annual_cycle',time.time() - start_time); start_time = time.time()
	context['fl_plot_location'] = plot_location(session,COU,mask)
	print('plot_location',time.time() - start_time); start_time = time.time()
	#print('max RAM usage %s' %(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 10**6))

	context.update(session)

	gc.collect()

	return render_template('choices.html',**context)


###############################
# option choices
###############################

@app.route('/indicator_choice',    methods=('POST', ))
def indicator_choice():
	form_indicator = forms.indicatorForm(request.form)
	session['indicator']=form_indicator.indicators.data
	return redirect(url_for('choices'))

@app.route('/season_choice',    methods=('POST', ))
def season_choice():
	form_season = forms.seasonForm(request.form)
	session['season']=form_season.seasons.data
	return redirect(url_for('choices'))

@app.route('/region_choice',    methods=('POST', ))
def region_choice():
	form_region = forms.regionForm(request.form)
	session['region']=form_region.regions.data

	COU=class_on_shape.shaped_object(iso=session['country'], working_directory='../small_data/'+session['country'])
	COU.load_shapefile()
	area=COU.get_region_area(session['region'])['latxlon']*4
	if area<4:
		session['small_region_warning']=True
	else:
		session['small_region_warning']=False

	return redirect(url_for('choices'))

@app.route('/period_choice',    methods=("POST", ))
def period_choice():
	form_period = forms.PeriodField(request.form)
	period_ref=[int(form_period.period_ref.data.split("-")[0]),int(form_period.period_ref.data.split("-")[1])]
	session["period_ref"]     = period_ref
	period_proj=[int(form_period.period_proj.data.split("-")[0]),int(form_period.period_proj.data.split("-")[1])]
	session["period_proj"]    = period_proj

	session['period_ref_warning']='ok'
	if period_ref[1]-period_ref[0]<20:    session['period_ref_warning']='small'
	if period_ref[0]>period_ref[1]:    session['period_ref_warning']='strange'
	if period_ref[1]>2006:    session['period_ref_warning']='out_range'
	if period_ref[0]<1979:    session['period_ref_warning']='out_range'

	session['period_proj_warning']='ok'
	if period_proj[1]-period_proj[0]<20:    session['period_proj_warning']='small'
	if period_proj[0]>period_proj[1]:    session['period_proj_warning']='strange'
	if period_proj[1]>2100:    session['period_proj_warning']='out_range'
	if period_proj[0]<1950:    session['period_proj_warning']='out_range'

	return redirect(url_for("choices"))

@app.route('/warming_lvl_choice',  methods=('POST', ))
def warming_lvl_choice():
	form_warming_lvl_ref = forms.warming_lvl_refForm(request.form)
	session['wlvl_ref']=form_warming_lvl_ref.warming_lvl_refs.data

	form_warming_lvl = forms.warming_lvlForm(request.form)
	session['wlvl']=form_warming_lvl.warming_lvls.data
	return redirect(url_for('choices'))

@app.route('/switch_to_periods')#,  methods=("POST", )
def switch_to_periods():
  session['use_periods']=abs(session['use_periods']-1)
  return redirect(url_for("choices"))


@app.route('/prepare_for_download/<plot_request>',  methods=('GET',"POST", ))
def prepare_for_download(plot_request):
	print(plot_request)
	request_type=plot_request.split('**')[0]
	plot_format=plot_request.split('**')[-1]

	start_time = time.time()
	COU=class_on_shape.shaped_object(iso=session['country'], working_directory='../small_data/'+session['country'])
	COU.load_shapefile()
	COU.meta_load()
	COU.griddedData_read(tags={'var_name':[session['indicator']]})
	if session['indicator'] in ['tas','TXx']:
		COU.griddedData_unit_conversion(shift=-273.15)

	COU.load_mask()
	mask = COU._masks[list(COU._data.keys())[0]]['latWeight'].loc[session['region']]
	#print(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)
	print('loaded',time.time() - start_time); start_time = time.time()

	plt.close('all')
	if request_type=='plot_reference_map':  filename=plot_reference_map(session,COU,mask,out_format=plot_format)
	if request_type=='plot_projection_diff_map':  filename=plot_projection_diff_map(session,COU,mask,out_format=plot_format)
	if request_type=='plot_transient':  filename=plot_transient(session,COU,mask,out_format=plot_format)
	if request_type=='plot_annual_cycle':  filename=plot_annual_cycle(session,COU,mask,out_format=plot_format)
	plt.clf()

	return send_from_directory(directory=os.path.abspath(os.getcwd())+'/app/', filename=filename.replace('app/',''),as_attachment=True)



###############################
# Regions
###############################

@app.route('/merging_page')
def merging_page():

	print(0000000)
	print(session['regions_to_merge'],len(session['regions_to_merge']))
	if len(session['regions_to_merge']) == 0:
		if session['region'] == session['country']:
			session['regions_to_merge'] = []
			session['new_region_name'] = ''
		else:
			session['regions_to_merge'] = [session['region']]
			session['new_region_name'] = session['region']



	COU=class_on_shape.shaped_object(iso=session['country'], working_directory='../small_data/'+session['country'])
	COU.load_shapefile()
	COU.load_mask()
	COU.meta_load()

	mask = COU._masks[list(COU._masks.keys())[0]]['overlap']

	asp=(float(len(mask.lon))/float(len(mask.lat)))**0.5

	regions_plot='app/static/COU_images/'+session['country']+'/_'+session['new_region_name']+'.png'
	if os.path.isfile(regions_plot)==False:
		fig,ax = plt.subplots(nrows=1, frameon=False, subplot_kw={'projection': ccrs.PlateCarree()})
		fig.set_size_inches(6*asp,6/asp)

		ax.coastlines(resolution='10m');
		im = ax.pcolormesh(mask.lon,mask.lat,mask[0], alpha=0, transform=ccrs.PlateCarree())
		for name,shape in COU._region_polygons.items():
			ax.add_geometries([shape], ccrs.PlateCarree(), edgecolor='k',alpha=1,facecolor='none',linewidth=0.5,zorder=50)
			if name!=session['country'] and '+' not in name:
				x,y = shape.centroid.xy
				ax.annotate(COU._region_names[name], xy=(x[0],y[0]),ha='center',va='center')

		for reg in session['regions_to_merge']:
			ax.add_geometries([COU._region_polygons[reg]], ccrs.PlateCarree(), edgecolor='k',alpha=0.7,facecolor='orange',linewidth=0.5,zorder=2)
			ax.set_axis_off()
		plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
		plt.savefig(regions_plot,dpi=300)

	if session['region'] == session['country']:
		choosable_regions = [session['region']] + [reg for reg in COU._region_names.keys() if reg!=session['region'] and len(reg.split('+'))<2]
	else:
		choosable_regions = [session['region']] + [reg for reg in COU._region_names.keys() if reg!=session['country'] and reg!=session['region'] and len(reg.split('+'))<2]
	form_region = forms.regionForm(request.form)
	form_region.regions.choices = list(zip(choosable_regions,[COU._region_names[reg].replace('_',' ') for reg in choosable_regions]))

	form_NewRegion = forms.NewRegionForm(request.form)
	form_NewRegion = forms.NewRegionForm(request.form, region_name=session['new_region_name'])

	half_lon_step=abs(np.diff(mask.lon.copy(),1)[0]/2)
	half_lat_step=abs(np.diff(mask.lat.copy(),1)[0]/2)

	xmin,xmax=min(mask.lon.values)-half_lon_step,max(mask.lon.values)+half_lon_step
	ymin,ymax=min(mask.lat.values)-half_lat_step,max(mask.lat.values)+half_lat_step

	x_w=500*asp
	y_h=500/asp

	clickable=[]

	for region in choosable_regions:
		if region != session['country']:
			poly=COU._region_polygons[region]
			if poly.geom_type == 'MultiPolygon':
				area=[]
				for subpoly in poly:
					area.append(subpoly.area)
				x,y=poly[area.index(max(area))].simplify(0.1).exterior.xy

			elif poly.geom_type == 'Polygon':
				x,y=poly.simplify(0.1).exterior.xy

			point_list=''
			for xx,yy in list(zip(x,y)):
				point_list+=str((xx-xmin)/(xmax-xmin)*x_w)+', '
				point_list+=str(y_h-(yy-ymin)/(ymax-ymin)*y_h)+', '

			clickable.append({'poly':point_list[:-2],'name':region})

	context = {
		'form_region':form_region,
		'form_NewRegion':form_NewRegion,
		'regions_plot':regions_plot.replace('app/',''),
		'small_region_warning':session['small_region_warning'],
		'regions':clickable,
		'x_width':x_w,
		'y_height':y_h,
	}
	context.update(clickable)

	session['location']='merging_page'
	return render_template('merging_page_en.html',**context)

@app.route('/clear_selection',  methods=("POST", ))
def clear_selection():
	session['region']=session['country']
	session['regions_to_merge'] = []
	session['new_region_name'] = ''
	session['new_region_name_auto'] = True
	return redirect(url_for("merging_page"))

@app.route('/merge_with_region_from_form',  methods=('POST', ))
def merge_with_region_from_form():
	form_region = forms.regionForm(request.form)
	session['regions_to_merge'] = list(np.unique(session['regions_to_merge'] + [form_region.regions.data]))
	if session['new_region_name_auto']:
		session['new_region_name'] = '+'.join(session['regions_to_merge'])
	print(form_region.regions.data,session['regions_to_merge'],session['new_region_name'])
	return redirect(url_for('merging_page'))

@app.route('/merge_with_region_click/<region>',  methods=('POST', 'GET',))
def merge_with_region_click(region):
	session['regions_to_merge'] = list(np.unique(session['regions_to_merge'] + [region]))
	if session['new_region_name_auto']:
		session['new_region_name'] = '+'.join(session['regions_to_merge'])
	return redirect(url_for('merging_page'))

@app.route('/save_this_region',  methods=("POST", ))
def save_this_region():
	COU=class_on_shape.shaped_object(iso=session['country'], working_directory='../small_data/'+session['country'])
	COU.load_shapefile()
	COU.load_mask()
	COU.meta_load()

	form_NewRegion = forms.NewRegionForm(request.form)
	given_region_name=form_NewRegion.region_name.data
	print(given_region_name)

	print('done', session['regions_to_merge'], session['new_region_name'])
	COU.merge_regions(session['regions_to_merge'], session['new_region_name'], given_region_name=given_region_name)
	COU.load_mask()
	COU.create_masks_latweighted()
	session['region'] = session['new_region_name']

	session['regions_to_merge'] = []
	session['new_region_name'] = ''
	session['new_region_name_auto'] = True

	return redirect(url_for("choices"))


###############################
# Seasonal
###############################

@app.route('/season_page')
def season_page():

	new_season_name = '+'.join([str(sea) for sea in sorted(session['new_season'])])

	form_season = forms.seasonForm(request.form)
	months_avail = [str(i) for i in range(1,13)]
	form_season.seasons.choices = list(zip(months_avail,[settings.season_dict[sea]['name'] for sea in months_avail]))

	form_NewSeason = forms.NewSeasonForm(request.form)
	form_NewSeason = forms.NewSeasonForm(request.form, season_name='+'.join([settings.season_dict[str(sea)]['name'] for sea in sorted(session['new_season'])]))

	context = {
	  'form_season':form_season,
	  'form_NewSeason':form_NewSeason,
	  'new_season_name':new_season_name,
	  'months':[settings.season_dict[str(sea)]['name'] for sea in session['new_season']],
	}

	session['location']='season_page'
	return render_template('season_page_en.html',**context)

@app.route('/go_to_season_page')
def go_to_season_page():
	session['new_season']=[]
	session['new_season_name'] = ''
	session['new_season_name_auto'] = True
	return redirect(url_for("season_page"))

@app.route('/add_month',  methods=('POST', ))
def add_month():
	form_season = forms.seasonForm(request.form)
	session['new_season']+=[form_season.seasons.data]
	session['new_season']=sorted(set(session['new_season']))

	return redirect(url_for('season_page'))

@app.route('/save_this_season',  methods=("POST", ))
def save_this_season():
	season_name='+'.join([str(sea) for sea in sorted(session['new_season'])])

	form_NewSeason = forms.NewSeasonForm(request.form)
	given_name = form_NewSeason.season_name.data

	settings.season_dict[season_name] = dict(months=[int(i) for i in session['new_season']], name=given_name)
	session['season']=season_name

	return redirect(url_for("choices"))



###############################
# Navigation
###############################

@app.route('/go_to_choices',    methods=("POST", ))
def go_to_choices():
	return redirect(url_for("choices"))

@app.route('/home',    methods=('GET', ))
def render_home():
	return redirect(url_for('index'))

@app.route('/about',    methods=('GET', ))
def render_about():
	return render_template('about.html')

@app.route('/contact',    methods=('GET', ))
def render_contact():
	return render_template('contact.html')

@app.route('/documentation')
def documentation():
	return render_template('documentation.html')

@app.route('/error')
def error():
	return render_template('error.html')






#
