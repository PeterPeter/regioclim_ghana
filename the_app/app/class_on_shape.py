# -*- coding: utf-8 -*-
'''
Class to analyze climate data on national (& sub national) scale
'''
# Author: Peter Pfleiderer <peter.pfleiderer@climateanalytics.org>
#
# License: GNU General Public License v3.0

import sys,glob,os,pickle,time,gc
from collections import Counter
import numpy as np
import xarray as xr
#import xesmf as xe
import pandas as pd
from shapely.geometry import mapping, Polygon, MultiPolygon, asShape
from shapely.ops import cascaded_union, unary_union
import matplotlib.pylab as plt
import cartopy
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.io.shapereader as shapereader
import threading
import itertools

os.environ['OPENBLAS_NUM_THREADS'] = '1'

def save_pkl(obj, name ):
	with open(name, 'wb') as f:
		pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_pkl(name ):
	with open( name, 'rb') as f:
		return pickle.load(f)

class shaped_object(object):

	def __init__(self, iso, working_directory):
		'''
		Prepare directories and meta-data
		iso: str: Country name or iso
		working_directory: path: Path where files will be stored
		seasons: dict: seasons relevant for the country. 'season name':{months in season as int 1-12}
		additional_tag: str: when specified raw data will be stored in a separate directory with additional tag name.
		'''
		self._iso=iso
		self._working_dir=working_directory+'/'
		self._support_dir= '/'+'/'.join(working_directory.split('/')[:-1])+'/support/'

		if os.path.isdir(self._working_dir)==False: os.system('mkdir '+self._working_dir)
		if os.path.isdir(self._working_dir+'/masks')==False: os.system('mkdir '+self._working_dir+'/masks')
		if os.path.isdir(self._working_dir+'/raw_data')==False: os.system('mkdir '+self._working_dir+'/raw_data')
		if os.path.isdir(self._working_dir+'/griddedData')==False:os.system('mkdir '+self._working_dir+'/griddedData')
		if os.path.isdir(self._working_dir+'/areaAverage')==False:os.system('mkdir '+self._working_dir+'/areaAverage')
		if os.path.isdir(self._working_dir+'/tmp')==False:os.system('mkdir '+self._working_dir+'/tmp')

		self._masks={}
		self._region_names = {}
		self._region_polygons={}
		self._grid_dict = {}

	def download_shapefile(self):
		'''
		to be tested
		'''
		current_dir=os.getcwd()
		os.chdir(self._working_dir)
		os.system('mkdir '+self._iso+'_adm_shp')
		os.system('wget biogeo.ucdavis.edu/data/gadm3.6/shp/gadm36_'+self._iso+'_shp.zip')
		os.chdir(self._working_dir + self._iso+'_adm_shp')
		os.system('unzip ../gadm36_'+self._iso+'_shp.zip')
		os.chdir(current_dir)

	def read_shapefile(self, shape_file, long_name=None, short_name=None):
		adm_shapefiles=shapereader.Reader(shape_file).records()

		if long_name is None:
			print('Please specify which variables should be used as "long_name" and as "short_name"')
			for item in adm_shapefiles:
				shape,region=item.geometry,item.attributes
				print(item.attributes)
				return 0

		# collect all shapes of region
		for item in adm_shapefiles:
			shape,region=item.geometry,item.attributes
			name_full = u''+region[long_name].replace('_','-')
			name=u''+region[short_name].replace('_','-')

			try:
				tmp=MultiPolygon(shape)
			except:
				tmp=Polygon(shape)

			if name not in self._region_names.keys():
				self._region_names[name]=name_full
				self._region_polygons[name] = tmp

			else:
				self._region_polygons[name] = cascaded_union((self._region_polygons[name],tmp))

		save_pkl(self._region_names, self._working_dir+'region_names.pkl')
		save_pkl(self._region_polygons, self._working_dir+'region_polygons.pkl')

	def load_shapefile(self):
		self._region_names = load_pkl(self._working_dir+'region_names.pkl')
		self._region_polygons = load_pkl(self._working_dir+'region_polygons.pkl')


	##########
	# MASKS
	##########

	def load_mask(self):
		for mask_file in glob.glob(self._working_dir+'/masks/*.nc4'):
			small_grid = mask_file.split('_')[-2]
			if small_grid not in self._masks.keys():
				self._masks[small_grid]={}

			mask_style = mask_file.split('_')[-1].split('.')[0]
			nc_mask = xr.load_dataset(mask_file)
			tmp = xr.concat((nc_mask[var] for var in nc_mask.variables if var not in ['lon','lat','region']), dim='region').sortby(['lat', 'lon'])
			tmp = tmp.assign_coords(region=[var for var in nc_mask.variables if var not in ['lon','lat','region']])
			self._masks[small_grid][mask_style] = tmp.rename('mask')

			self._grid_dict[nc_mask.attrs['original_grid']] = small_grid
			nc_mask.close()

	def identify_grid(self,input_file,lat_name='lat',lon_name='lon'):
		'''
		get information about grid of input data
		input_file: file_path: file to be analyzed
		lat_name: str: name of latitude variable
		lon_name: str: name of longitude variable
		'''
		nc_in = xr.open_dataset(input_file)
		lat = nc_in[lat_name][:].values
		lon = nc_in[lon_name][:].values

		if len(lat.shape)==2:
			lat=lat[:,0]
			lon=lon[0,:]

		if max(lon)>200:	lon_shift=-180.0
		else:				lon_shift=0.0
		lon+=lon_shift

		nx = len(lon)	;	ny = len(lat)
		grid=str(str(ny)+'x'+str(nx)+'lat'+str(lat[0])+'to'+str(lat[-1])+'lon'+str(lon[0])+'to'+str(lon[-1])).replace('.','p')
		nc_in.close()

		# self._raw_lon = lon.values
		# self._raw_lat = lat.values
		# self._raw_grid = grid
		# slef._raw_lon_shift = lon_shift

		return lon,lat,grid,lon_shift

	def get_grid_polygons(self,grid,lon,lat,lon_shift):
		'''
		create polygons for each grid-cell
		grid: str: name of the grid
		lon: array: longitudes
		lat: array: latitudes
		lon_shift: float: deg longitudes that have to be shifted to be on a -180 to 180 grid (computed in identify_grid)
		'''
		# loop over the grid to get grid polygons
		nx = len(lon)	;	ny = len(lat)

		grid_polygons = np.empty((nx,ny),dtype=Polygon)
		dx = np.zeros((nx))
		dy = np.zeros((ny))
		dx[1:] = np.abs(np.diff(lon,1))
		dx[0] = dx[1]
		dy[1:] = np.abs(np.diff(lat,1))
		dy[0] = dy[1]
		for i in range(nx):
			x1 = lon[i]-dx[i]/2.
			x2 = lon[i]+dx[i]/2.
			for j in range(ny):
				y1 = lat[j]-dy[j]/2.
				y2 = lat[j]+dy[j]/2.
				grid_polygons[i,j] = Polygon([(x1,y1),(x1,y2),(x2,y2),(x2,y1)])
				#grid_polygons[i,j] = Polygon([(y1,x1),(y1,x2),(y2,x2),(y2,x1)])

		# since the lon axis has been shifted, masks and outputs will have to be shifted as well. This shift is computed here
		lon-=lon_shift
		shift = len(lon)-np.where(lon==lon[0]-lon_shift)[0][0]

		return grid_polygons,shift

	def grid_polygon_overlap(self,grid,lon,lat,grid_polygons,country_polygons,shift,ext_poly):
		'''
		Compute overlap betwwen grid polygons (get_grid_polygons) and country polygons
		grid: str: name of the grid
		lon: array: longitudes
		lat: array: latitudes
		grid_polygons: list: List of polygons created in get_grid_polygons
		country_polgons: list: list of polygons representing the country
		shift: int: number of elements to roll around longitude axis. Has to be considered since the longitude axis might be shifted (see get_grid_polygons)
		mask_style: str: Can be 'lat_weighted' or population weighted. If population weighted, mask_style is a given name
		est_poly: Polygon: Polygon limiting the are where overlaps are computed
		name: str: country-name or region name
		add_mask: np.array: population mask from regrid_add_mask
		'''
		nx = len(lon)	;	ny = len(lat)

		overlap = np.zeros((ny,nx))
		for i in range(nx):
			for j in range(ny):
				# check gridcell is relevant
				if grid_polygons[i,j].intersects(ext_poly):
					# get fraction of grid-cell covered by polygon
					overlap[j,i] = grid_polygons[i,j].intersection(country_polygons).area

		if np.nansum(overlap.copy().flatten())!=0:
			# mask zeros
			overlap[overlap==0]=np.nan
			overlap=np.ma.masked_invalid(overlap)
			# shift back to original longitudes
			return np.roll(overlap,shift,axis=1)
		else:
			print('something went wrong with the mask')
			return False

	def create_masks_overlap(self,input_file,lat_name='lat',lon_name='lon', verbose=True, region_names=None):
		'''
		create country mask
		input_file: str: location of example input data (required for the identification of the grid)
		shape_file: str: location of the shape_file used to identify country borders
		mask_style: str: name under which the mask will be stored (important for further analysis)
		add_mask_file: str: location of population mask (netcdf file) used for population weighted country mask
		overwrite: bool: if True, old files is deleted, new mask created
		lat_name: str: name of latitude variable in netcdf file
		lon_name: str: name of longitude variable in netcdf file
		'''

		lon,lat,grid,lon_shift = self.identify_grid(input_file,lat_name,lon_name)

		grid_polygons,shift = self.get_grid_polygons(grid,lon,lat,lon_shift)

		# get boundaries for faster computation
		if region_names is None:
			region_names = list(self._region_polygons.keys())

		ext_poly = self._region_polygons[region_names[0]]
		if len(region_names) > 0:
			for region_name in region_names[1:]:
				ext_poly = cascaded_union((ext_poly,self._region_polygons[region_name]))
		x1, y1, x2, y2 = ext_poly.bounds
		xmin, xmax, ymin, ymax = min([x1,x2]), max([x1,x2]), min([y1,y2]), max([y1,y2])
		ext = [(xmin,ymin),(xmin,ymax),(xmax,ymax),(xmax,ymin),(xmin,ymin)]

		out_file = self._working_dir+'/masks/'+self._iso+'_'+grid+'_overlap.nc4'
		if os.path.isfile(out_file):
			nc_out_mask = xr.load_dataset(out_file)
		else:
			nc_out_mask = xr.Dataset({})

		# compute overlap
		for region_name in region_names:
			polygons = self._region_polygons[region_name]
			if verbose:
				print(region_name)
			tmp = self.grid_polygon_overlap(grid, lon, lat, grid_polygons, polygons, shift, ext_poly)

			# preprare output
			nc_out_mask[region_name] = xr.DataArray(data=tmp, coords={'lat':lat,'lon':lon}, dims=['lat','lon'])
			nc_out_mask.to_netcdf(out_file)

		# get relevant extend
		lat_,lon_ = [],[]
		for region_name in [reg for reg in nc_out_mask.variables if reg not in ['lat','lon']]:
			lons=sorted(np.where(np.isfinite(np.nanmean(nc_out_mask[region_name],0)))[0])
			lon_ += list(lon[lons[0]:lons[-1]+1])
			lats=sorted(np.where(np.isfinite(np.nanmean(nc_out_mask[region_name],1)))[0])
			lat_ += list(lat[lats[0]:lats[-1]+1])
		lat_ = np.unique(lat_)
		lon_ = np.unique(lon_)

		nc_out_mask = nc_out_mask.sel({'lat':lat_,'lon':lon_})

		small_grid=str(str(len(lat_))+'x'+str(len(lon_))+'lat'+str(lat_[0])+'to'+str(lat_[-1])+'lon'+str(lon_[0])+'to'+str(lon_[-1])).replace('.','p')
		self._grid_dict[grid] = small_grid

		nc_out_mask.attrs['original_grid'] = grid
		nc_out_mask.attrs['grid'] = small_grid
		nc_out_mask.attrs['mask_style'] = 'overlap'
		nc_out_mask.to_netcdf(out_file)

		self.load_mask()

	def create_masks_latweighted(self):
		for grid in self._masks.keys():
			mask = self._masks[grid]['overlap']
			lat_weight = np.cos(np.deg2rad(mask.lat))
			lat_weight_array = np.repeat(lat_weight.values[np.newaxis,:], len(mask.lon), 0).T

			nc_out_mask = xr.Dataset({})

			tmp = mask * lat_weight_array
			for region_name in mask.region.values:
				tmp.loc[region_name] /= tmp.loc[region_name].sum()
				nc_out_mask[region_name] = tmp.loc[region_name]

			nc_out_mask.attrs['original_grid'] = grid
			nc_out_mask.attrs['grid'] = self._grid_dict[grid]
			nc_out_mask.attrs['mask_style'] = 'latWeight'
			nc_out_mask.to_netcdf(self._working_dir+'/masks/'+self._iso+'_'+grid+'_latWeight.nc4')

	def create_masks_other_weight(self, weight, name):
		for grid in self._masks.keys():
			mask = self._masks[grid]['overlap']

			refrence_grid = xr.Dataset({'lat': (['lat'], mask.lat),'lon': (['lon'], mask.lon),})
			regridder = xe.Regridder(weight, refrence_grid, 'bilinear', reuse_weights=True, filename=self._working_dir+'/tmp/regrid_'+grid+'.nc')
			weight_regridded = regridder(weight)

			nc_out_mask = xr.Dataset({})

			tmp = mask * weight_regridded
			for region_name in mask.region.values:
				tmp.loc[region_name] /= tmp.loc[region_name].sum()
				nc_out_mask[region_name] = tmp.loc[region_name]

			nc_out_mask.attrs['original_grid'] = grid
			nc_out_mask.attrs['grid'] = self._grid_dict[grid]
			nc_out_mask.attrs['mask_style'] = name
			nc_out_mask.to_netcdf(self._working_dir+'/masks/'+self._iso+'_'+grid+'_'+name+'.nc4')

	def merge_regions(self, region_names, new_region_name, given_region_name=None):
		self._region_polygons[new_region_name] = self._region_polygons[region_names[0]]
		if len(region_names) > 0:
			for region_name in region_names[1:]:
				self._region_polygons[new_region_name] = cascaded_union((self._region_polygons[new_region_name],self._region_polygons[region_name]))

		if given_region_name is None:
			self._region_names[new_region_name] = new_region_name
		else:
			self._region_names[new_region_name] = given_region_name

		for grid in self._masks.keys():
			mask = self._masks[grid]['overlap']

			tmp = mask.loc[region_names[0]].copy()
			tmp.values[:] = 0
			for region_name in region_names:
				print(region_name)
				tmp__ = mask.loc[region_name]
				tmp__.values[np.isnan(tmp__.values)] = 0
				tmp += tmp__
				print(tmp.sum().values)
				print(tmp__.sum().values)
			tmp.values[tmp.values==0] = np.nan

			print(new_region_name)
			nc_out_mask = xr.load_dataset(self._working_dir+'/masks/'+self._iso+'_'+grid+'_overlap.nc4')
			nc_out_mask[new_region_name] = tmp
			nc_out_mask.to_netcdf(self._working_dir+'/masks/'+self._iso+'_'+grid+'_overlap.nc4')

		save_pkl(self._region_names, self._working_dir+'region_names.pkl')
		save_pkl(self._region_polygons, self._working_dir+'region_polygons.pkl')

	def get_region_area(self,region):
		poly=self._region_polygons[region]
		lat=poly.centroid.xy[1][0]
		return({'km2':poly.area*(12742./360.)**2*np.cos(np.radians(lat))*10,'latxlon':poly.area})

	def zoom_data(self,input_file,var_name,given_var_name=None,tags={}, lat_name='lat', lon_name='lon', time_cutoff=None, input_array=None, overwrite=False, check_exist=False, storage='griddedData'):
		lon,lat,grid,lon_shift = self.identify_grid(input_file,lat_name,lon_name)

		tag = ''
		for key,val in tags.items():
			tag += '_'+key+'-'+val
		out_file_name = self._working_dir+'/'+storage+'/'+self._iso+'_'+self._grid_dict[grid]+'_'+given_var_name+tag+'.nc4'

		if os.path.isfile(out_file_name) and overwrite==False:
			print('already exists')
			return 'already exists'
		if check_exist:
			return 'does not exist'

		if input_array is None:
			_input = xr.open_dataset(input_file)[var_name].squeeze()
		else:
			_input = input_array

		mask = self._masks[self._grid_dict[grid]][list(self._masks[self._grid_dict[grid]].keys())[0]]

		# clean data
		_input = _input.assign_coords(time=np.asarray(_input.time.values,'datetime64[s]'))
		med_diff = np.median(np.diff(_input.time,1))
		time_steps = np.array([0] + [i+1 for i,tt in enumerate(np.diff(_input.time,1)) if np.abs(int(tt) - med_diff) < med_diff])
		_input = _input[time_steps,:,:]

		# select
		if time_cutoff is not None:
			_input = _input.sel(time=slice(time_cutoff[0],time_cutoff[1]))
			if len(_input.time) == 0:
				print('no useful time steps')
				return 'useless'

		# zoom
		refrence_grid = xr.Dataset({'time': (['time'], _input.time),'lat': (['lat'], mask.lat),'lon': (['lon'], mask.lon),})
		regridder = xe.Regridder(_input, refrence_grid, 'bilinear', reuse_weights=True, filename=self._working_dir+'/tmp/'+self._iso+'_'+self._grid_dict[grid]+'_'+given_var_name+tag+'.nc4')
		zoomed_data = regridder(_input)

		if given_var_name is None:
			given_var_name = var_name
		out_data = xr.Dataset({given_var_name: zoomed_data})
		out_data.attrs['original_grid'] = grid
		out_data.attrs['grid'] = self._grid_dict[grid]
		out_data.attrs['var_name_original'] = var_name

		for key,val in tags.items():
			out_data[given_var_name].attrs['tag_'+key] = val
		out_data.to_netcdf(out_file_name, format='NETCDF4')
		print('done')
		return 'done'

	def harmonize_time(self,tmp_time):
		_, index = np.unique(tmp_time, return_index=True)
		if np.median(np.diff(tmp_time[index].dt.year,1)[1:]) == 1: # yearly
			tmp_time = np.array([np.datetime64(str(dd)[:4]+'-06-15T00:00:00.000000000') for dd in tmp_time.values])
			time_format = 'yearly'
		elif np.median(np.diff(tmp_time[index].dt.month,1)[1:]) == 1: # monthly
			tmp_time = np.array([np.datetime64(str(dd)[:8]+'15T00:00:00.000000000') for dd in tmp_time.values])
			time_format = 'monthly'
		elif np.median(np.diff(tmp_time[index].dt.day,1)[1:]) == 1: # monthly
			tmp_time = np.array([np.datetime64(str(dd)[:10]+'T00:00:00.000000000') for dd in tmp_time.values])
			time_format = 'daily'
		else:
			time_format = None
			print('time format not yearly, not monthly, not daily -> unknown')

		return tmp_time,time_format, index

	def meta_gather_information(self, ignore_tags=[], storage='griddedData'):
		start = time.time()
		# get information about all files
		grids = np.unique([nn.split('_')[-2] for nn in glob.glob(self._working_dir+'/masks/*.nc4')])
		self._tags = ['var_name']
		file_dict_gridded = {grid:{} for grid in grids}
		coords_dict = {grid:{'monthly':{'var_name':[]},'yearly':{'var_name':[]},'daily':{'var_name':[]}} for grid in grids}
		coords_dict_area = {'monthly':{'var_name':[]},'yearly':{'var_name':[]},'daily':{'var_name':[]}}
		time_formats = []
		for file_name in glob.glob(self._working_dir+'/'+storage+'/*.nc4'):
			var_name = file_name.split('/')[-1].split('_')[2]
			nc_in = xr.open_dataset(file_name, cache=False)

			grid = nc_in.attrs['grid']

			# identify and harmonize time format
			tmp_time,time_format,index = self.harmonize_time(nc_in['time'])

			# store coordinates
			if time_format is not None:
				time_formats.append(time_format)
				tmp_time = xr.DataArray(tmp_time, coords={'time':tmp_time}, dims=['time'])
				if 'time' not in coords_dict[grid][time_format].keys():
					coords_dict[grid][time_format]['time'] = tmp_time
					coords_dict_area[time_format]['time'] = tmp_time
				else:
					ttt = xr.concat([coords_dict[grid][time_format]['time'],tmp_time], dim='time')
					_, index = np.unique(ttt['time'], return_index=True)
					coords_dict[grid][time_format]['time'] = ttt.isel(time=index)
					coords_dict_area[time_format]['time'] = ttt.isel(time=index)
				if 'lon' not in coords_dict[grid][time_format].keys():
					coords_dict[grid][time_format]['lon'] = nc_in['lon']
				if 'lat' not in coords_dict[grid][time_format].keys():
					coords_dict[grid][time_format]['lat'] = nc_in['lat']

				# store tags
				tag_combi = {'var_name':var_name}
				for key,val in nc_in[var_name].attrs.items():
					if 'tag' in key and key not in ['tag_'+igTag for igTag in ignore_tags]:
						tag_combi[key.split('_')[-1]] = val
						if key.split('_')[-1] not in self._tags:
							self._tags += [key.split('_')[-1]]
						# for each grid
						if key.split('_')[-1] not in coords_dict[grid][time_format].keys():
							coords_dict[grid][time_format][key.split('_')[-1]] = []
						if val not in coords_dict[grid][time_format][key.split('_')[-1]]:
							coords_dict[grid][time_format][key.split('_')[-1]].append(val)
						# irrespective of grid
						if key.split('_')[-1] not in coords_dict_area[time_format].keys():
							coords_dict_area[time_format][key.split('_')[-1]] = []
						if val not in coords_dict_area[time_format][key.split('_')[-1]]:
							coords_dict_area[time_format][key.split('_')[-1]].append(val)
				if var_name not in coords_dict[grid][time_format]['var_name']:
					coords_dict[grid][time_format]['var_name'] += [var_name]

				if var_name not in coords_dict_area[time_format]['var_name']:
					coords_dict_area[time_format]['var_name'] += [var_name]

				if time_format not in file_dict_gridded[grid].keys():
					file_dict_gridded[grid][time_format] = {}
				file_dict_gridded[grid][time_format][file_name.split('/')[-1]] = tag_combi

		# # reduce complexity if the grid is always the same or time formats are always the same
		# if len(np.unique(time_formats)) == 1:
		# 	for grid,tmp_dict in coords_dict.items():
		# 		file_dict_gridded[grid] = file_dict_gridded[grid][time_format]
		# 		coords_dict[grid] = coords_dict[grid][time_format]
		#
		# if len(grids) == 1:
		# 	file_dict_gridded = file_dict_gridded[grid]
		# 	coords_dict = coords_dict[grid]


		self._meta = {
			'coords_dict_gridded' : coords_dict,
			'coords_dict_area' : coords_dict_area,
			'file_dict_gridded' : file_dict_gridded,
			'grids' : grids,
			'time_formats' : np.unique(time_formats),
		}

		save_pkl(self._meta, self._working_dir+'meta_info.pkl')
		print(time.time() - start); start = time.time()

	def meta_load(self):
		self._meta = load_pkl(self._working_dir+'meta_info.pkl')

	def griddedData_read(self, tags={}, storage='griddedData'):
		start = time.time()
		coords_dict = self._meta['coords_dict_gridded']
		file_dict_gridded = self._meta['file_dict_gridded']

		# create empty data arrays
		self._data = {grid:{} for grid in self._meta['grids']}
		for grid in self._meta['grids']:
			# for each grid
			for time_format in self._meta['time_formats']:

				coords = coords_dict[grid][time_format]
				# for each time format
				if len(coords['var_name']) > 0:

					# define coords
					for key,val in coords.items():
						if key in tags.keys():
							# either just a selcetion
							coords[key] = tags[key]
						else:
							# or all available
							coords[key] = np.unique(val)

					dims = ['time','lat','lon']
					for key in sorted(coords.keys()):
						if key not in dims:
							dims = [key] + dims

					coords = {dim:coords[dim] for dim in dims}
					self._data[grid][time_format] = xr.DataArray(coords=coords, dims=dims)

		# fill data arrays
		for grid,time_format_dict in file_dict_gridded.items():
			for time_format, file_dict in time_format_dict.items():
				for file_name, tag_dict in file_dict.items():

					# check whether file is required by looking if tags of file match coords
					if np.all([tag_dict[dim] in coo for dim,coo in self._data[grid][time_format].coords.items() if dim not in ['time','lat','lon']]):
						# read file
						nc_in = xr.open_dataset(self._working_dir+'/'+storage+'/'+file_name)

						# extract var_name
						var_name = file_name.split('/')[-1].split('_')[2]

						# format time
						tmp_time,time_format,index = self.harmonize_time(nc_in['time'])

						# useless?
						nc_in['time'] = tmp_time
						nc_in = nc_in.isel(time=index)

						indices = [tag_dict[dim] for dim  in self._data[grid][time_format].dims if dim not in ['time','lat','lon']]

						'''
						this is really ugly but I don't know how to make this differently
						'''
						if len(indices) == 0:
							self._data[grid][time_format].loc[nc_in.time,nc_in.lat,nc_in.lon] = nc_in[var_name]
						if len(indices) == 1:
							self._data[grid][time_format].loc[indices[0],nc_in.time,nc_in.lat,nc_in.lon] = nc_in[var_name]
						if len(indices) == 2:
							self._data[grid][time_format].loc[indices[0],indices[1],nc_in.time,nc_in.lat,nc_in.lon] = nc_in[var_name]
						if len(indices) == 3:
							self._data[grid][time_format].loc[indices[0],indices[1],indices[2],nc_in.time,nc_in.lat,nc_in.lon] = nc_in[var_name]
						if len(indices) == 4:
							self._data[grid][time_format].loc[indices[0],indices[1],indices[2],indices[3],nc_in.time,nc_in.lat,nc_in.lon] = nc_in[var_name]
						if len(indices) == 5:
							self._data[grid][time_format].loc[indices[0],indices[1],indices[2],indices[3],indices[4],nc_in.time,nc_in.lat,nc_in.lon] = nc_in[var_name]

				if len(list(file_dict.keys())) > 0:
					self._data[grid][time_format].sortby(['lat', 'lon','time'])

		print(time.time() - start); start = time.time()
		print('Loaded data into self._data')
		self.griddedData_print()

	def griddedData_store_after_reading(self, ignore_tags, storage='griddedData', from_storage='raw_data'):
		'''
		only makes sense if ignore_tags have been used in scout_data
		'''
		file_dict_gridded = self._meta['file_dict_gridded']

		for grid,time_format_dict in self._data.items():
			for time_format,data in time_format_dict.items():

				dims = [key for key in data.dims if key not in ['time','lat','lon']]
				for combi in list(itertools.product(*[data.coords[key].values for key in dims])):
					print(combi)
					tmp = data
					for coo in combi:
						tmp = tmp.loc[coo]

					tmp = tmp.loc[np.isfinite(np.nanmean(tmp, axis=(-1,-2)))].squeeze()

					if len(tmp.time) > 0:
						out_data = xr.Dataset({combi[0]:tmp})

						for file_name,coos in file_dict_gridded[grid][time_format].items():
							if np.isin(combi,list(coos.values())).sum() == len(combi):
								break
						nc_example = xr.open_dataset(self._working_dir+'/'+from_storage+'/'+file_name)

						out_data.attrs = nc_example.attrs
						out_data[combi[0]].attrs = {key:val for key,val in nc_example[combi[0]].attrs.items() if key not in ignore_tags}

						out_name = '_'.join([strr for strr in file_name.split('/')[-1].split('.nc')[0].split('_') if strr.split('-')[0] not in ignore_tags])

						out_data.to_netcdf(self._working_dir+'/'+storage+'/'+out_name+'.nc4', format='NETCDF4')

					else:
						print('!!!! missing combi')

					gc.collect()

	def griddedData_reduce(self):
		for grid,time_format_dict in self._data.items():
			for time_format, file_dict in time_format_dict.items():
				pass
			# remove cumbersom dict that is useless if only one time format exists
			if len(self._meta['time_formats']) == 1:
				self._data[grid] = self._data[grid][time_format]
		# remove cumbersom dict that is useless if only one grid exists
		if len(self._meta['grids']) == 1:
			self._data = self._data[grid]

	def griddedData_print(self):
		for grid, tmp1_dict in self._data.items():
			for time_format, tmp in tmp1_dict.items():
				print('grid: '+grid)
				print('time format: '+time_format)
				print(tmp.coords,'\n')

	def griddedData_unit_conversion(self, shift=0, factor=1):
		for grid, tmp1_dict in self._data.items():
			for time_format, tmp in tmp1_dict.items():
				tmp.values = tmp.values * factor + shift

	def calculate_areaAverage(self, regions=None):
		file_dict_gridded = self._meta['file_dict_gridded']
		coords_dict_area = self._meta['coords_dict_area']
		time_formats = np.array([[time_format for time_format in tmp1.keys()] for tmp1 in file_dict_gridded.values()]).flatten()

		area_average_meta = open(self._working_dir+'areaAverage/meta.txt' ,'a')

		if regions is None:
			regions = self._region_names.keys()

		for region in regions:
			if os.path.isdir(self._working_dir+'areaAverage/'+region) == False:
				os.mkdir(self._working_dir+'areaAverage/'+region)
			for time_format in time_formats:
				dims = [dim for dim in coords_dict_area[time_format].keys() if dim not in ['lat','lon','time']] + ['time']
				coords = {dim:coords_dict_area[time_format][dim]  for dim in dims}
				coords['mask_style'] = np.array([list(tmp1.keys()) for grid,tmp1 in self._masks.items()]).flatten()
				dims = ['mask_style'] + dims
				tmp_xr = xr.DataArray(data=np.zeros([len(coords[key]) for key in dims])*np.nan, coords=coords, dims=dims)


				for tag_combi_l in list(itertools.product(*[coords[dim] for dim in dims if dim not in ['mask_style','time']])):
					tag_combi = {key:val for key,val in zip([dim for dim in dims if dim not in ['mask_style','time']],tag_combi_l)}

					found = False
					for grid in [gr for gr in self._meta['grids'] if time_format in self._data[gr].keys()]:

						if np.all([tag_combi[dim] in coo for dim,coo in self._data[grid][time_format].coords.items() if dim not in ['time','lat','lon']]):
							area_average_meta.write(str(tag_combi_l)+' <-- '+grid+'\n')
							found = True
							break

					if found:
						tmp = self._data[grid][time_format].loc[tuple(tag_combi[dim] for dim in self._data[grid][time_format].dims[:-3])].copy()
						nans = np.isnan(tmp)

						for mask_style,mask in self._masks[grid].items():
							tmp *= mask.loc[region]
							av = tmp.sum(axis=(-2,-1))
							av.values[np.all(nans, axis=(1,2))] = np.nan

							tag_combi_ = tag_combi.copy()
							tag_combi_['region'] = region
							tag_combi_['mask_style'] = mask_style
							indices = [tag_combi_[dim] for dim  in tmp_xr.dims if dim not in ['time']]

							if len(indices) == 0:
								tmp_xr.loc[av.time] = av
							if len(indices) == 1:
								tmp_xr.loc[indices[0],av.time] = av
							if len(indices) == 2:
								tmp_xr.loc[indices[0],indices[1],av.time] = av
							if len(indices) == 3:
								tmp_xr.loc[indices[0],indices[1],indices[2],av.time] = av
							if len(indices) == 4:
								tmp_xr.loc[indices[0],indices[1],indices[2],indices[3],av.time] = av
							if len(indices) == 5:
								tmp_xr.loc[indices[0],indices[1],indices[2],indices[3],indices[4],av.time] = av
							if len(indices) == 6:
								tmp_xr.loc[indices[0],indices[1],indices[2],indices[3],indices[4],indices[5],av.time] = av

							tag = '_'.join([key+'-'+val for key,val in tag_combi_.items()])
							av.to_pandas().to_csv(self._working_dir+'areaAverage/'+region+'/'+region+'_'+time_format+'_'+tag+'.csv')

				if os.path.isfile(self._working_dir+'areaAverage/'+region+'_'+time_format+'_areaAverage.nc'):
					os.system('rm '+self._working_dir+'areaAverage/'+region+'_'+time_format+'_areaAverage.nc')
				xr.Dataset({'areaAverage':tmp_xr}).to_netcdf(self._working_dir+'areaAverage/'+region+'_'+time_format+'_areaAverage.nc')

		area_average_meta.close()

	def load_areaAverage(self):
		file_dict_gridded = self._meta['file_dict_gridded']
		self._areaAverage = {}
		time_formats = np.array([[time_format for time_format in tmp1.keys()] for tmp1 in file_dict_gridded.values()]).flatten()
		self._areaAverage = {}
		for time_format in time_formats:
			tmp = xr.open_mfdataset(self._working_dir+'areaAverage/*_'+time_format+'_areaAverage.nc', concat_dim='region', combine='nested')
			tmp.coords['region'] = [ff.split('/')[-1].split('_')[0] for ff in glob.glob(self._working_dir+'areaAverage/*_'+time_format+'_areaAverage.nc')]
			self._areaAverage[time_format] = tmp['areaAverage'].load()

	def select_areaAverage(self, time_format, tags):
		tmp = self._areaAverage[time_format]
		indices = []
		for key in tmp.dims[:-1]:
			if key in tags.keys():
				indices.append(tags[key])
			else:
				indices.append(tmp[key].values)
		return tmp.loc[tuple(indices)]

	def select_griddedData(self, time_format, tags):
		indices = []
		for grid,tmp in self._data.items():
			found_dims = []
			for key in tags.keys():
				if key in tmp[time_format].dims:
					found_dims.append(key)
			if len(found_dims) == len(list(tags.keys())):
				for key in tmp[time_format].dims[:-2]:
					if key in tags.keys():
						indices.append(tags[key])
					else:
						indices.append(tmp[time_format][key].values)
				return tmp[time_format].loc[tuple(indices)]

	def unit_conversion(self, time_format, var_name, addition=0, factor=1):
		tags = {'var_name':var_name}
		indices = []
		for grid,tmp in self._data.items():
			found_dims = []
			for key in tags.keys():
				if key in tmp[time_format].dims:
					found_dims.append(key)
			if len(found_dims) == len(list(tags.keys())):
				for key in tmp[time_format].dims[:-2]:
					if key in tags.keys():
						indices.append(tags[key])
					else:
						indices.append(tmp[time_format][key].values)

				print(indices)
				print(np.nanmedian(tmp[time_format].loc[tuple(indices)]))
				tmp[time_format].loc[tuple(indices)] += addition
				tmp[time_format].loc[tuple(indices)] *= factor
				print(np.nanmedian(tmp[time_format].loc[tuple(indices)]))






#
