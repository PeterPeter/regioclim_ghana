# -*- coding: utf-8 -*-

# Copyright (C) 2017 Peter Pfleiderer
#
# This file is part of regioclim.
#
# regioclim is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# regioclim is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License
# along with regioclim; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA




""" database setting file. """


import sys,glob,os,pickle,string
import numpy as np
import pandas as pd
import pycountry

seasons={'year':range(1,13)}
for i in range(1,13):
	seasons[str(i)]=[i]

all_isos=['GHA']

country_names={}
for iso in all_isos:
	if os.path.isdir('app/static/COU_images/'+iso)==False:
		os.system('mkdir app/static/COU_images/'+iso)
	country_names[iso]=pycountry.countries.get(alpha_3=iso).name

datasets=['CORDEX_BC']

indicator_dict={
	'tas':{'name':'Temperature','unit':'$^\circ$C','time_step':'monthly','cmap':{'div':'RdBu_r','pos':'Reds','neg':'Blues_r'}},
	'pr':{'name':'Precipitation','unit':'mm','time_step':'monthly','cmap':{'div':'RdBu','pos':'Blues','neg':'Reds_r'}},
	'RX1':{'name':'Wet extremes (RX1)','unit':'mm','time_step':'monthly','cmap':{'div':'RdBu','pos':'Blues','neg':'Reds_r'}},
	'TXx':{'name':'Hot extremes (TXx)','unit':'$^\circ$C','time_step':'monthly','cmap':{'div':'RdBu_r','pos':'Reds','neg':'Blues_r'}},
	'RX5':{'name':'5-day wet extremes (RX5)','unit':'mm','time_step':'yearly','cmap':{'div':'RdBu','pos':'Blues','neg':'Reds_r'}},
}

def period_name(period):
	return str(period[0])+'-'+str(period[1])


wlvls = pd.read_csv('../small_data/'+'GHA'+'/warming_levels.csv', index_col=None)
wlvls = wlvls[wlvls.columns[1:]]
model_mapping = {v: k for k, v in {'IPSL':'ipsl-cm5a-lr','HADGEM2':'hadgem2-es','ECEARTH':'ec-earth','MPIESM':'mpi-esm-lr'}.items()}
wlvls.columns = ['wlvl'] + [model_mapping[model.split('_')[0]] for model in wlvls.columns[1:]]
wlvls['wlvl'] = [str(w) for w in wlvls['wlvl']]
wlvls = wlvls.append({'wlvl':'0.61','HADGEM2':1996,'MPIESM':1996,'IPSL':1996,'ECEARTH':1996}, ignore_index=True)

wlvl_dict = {
	'1.5' : '+1.5°C above preindustrial',
	'2.0' : '+2.0°C above preindustrial',
	'0.61' : '1986-2005 (+0.61°C above preindustrial)',
}

season_dict = {}
season_dict['annual'] = dict(months=[1,2,3,4,5,6,7,8,9,10,11,12], name='Annual')
season_dict['1'] = dict(months=[1], name='January')
season_dict['2'] = dict(months=[2], name='February')
season_dict['3'] = dict(months=[3], name='March')
season_dict['4'] = dict(months=[4], name='April')
season_dict['5'] = dict(months=[5], name='Mai')
season_dict['6'] = dict(months=[6], name='June')
season_dict['7'] = dict(months=[7], name='July')
season_dict['8'] = dict(months=[8], name='August')
season_dict['9'] = dict(months=[9], name='September')
season_dict['10'] = dict(months=[10], name='October')
season_dict['11'] = dict(months=[11], name='November')
season_dict['12'] = dict(months=[12], name='December')


form_labels={'fr':{
	'country':u'Pays analysé:',
	'region':u'Région administrative:',
	'indicator':u'Indicateur climatique:',
	'season':u'Saison:',
	},'en':{
	'country':u'Studied country:',
	'region':u'Administrative region:',
	'indicator':u'Climate indicator:',
	'season':u'Season:',
	}

}



print('done with settings')
