
import os,sys, importlib, glob,gc
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd


sys.path.append('/home/pepflei/Projects/climate_data_on_shapes/')
import class_on_shape; importlib.reload(class_on_shape)

iso = 'GHA'


COU = class_on_shape.shaped_object(iso=iso, working_directory='/p/tmp/pepflei/regioClim_Ghana/'+iso)

COU.read_shapefile(COU._working_dir+'/'+iso+'_adm_shp/'+iso+'_adm0.shp')
COU.read_shapefile(COU._working_dir+'/'+iso+'_adm_shp/'+iso+'_adm0.shp', long_name='NAME_ISO', short_name='ISO')
COU.read_shapefile(COU._working_dir+'/'+iso+'_adm_shp/'+iso+'_adm1.shp')
COU.read_shapefile(COU._working_dir+'/'+iso+'_adm_shp/'+iso+'_adm1.shp', long_name='NAME_1', short_name='HASC_1')
COU.load_shapefile()

print('create mask first')
COU.create_masks_overlap(input_file='/p/projects/ikiimp/RCM_BC/ISIMIP2b_bc/GCMinput/monthly/tas/mon_tas_ECEARTH_historical_.nc4')
COU.create_masks_latweighted()

for var,old_varname in zip(['tas','pr','TXx','RX1','RX5'],['tas','pr','tasmax','pr','pr']):
	all_files=glob.glob('/p/projects/ikiimp/RCM_BC/ISIMIP2b_bc/GCMoutput/monthly/'+var+'/mon_'+var+'_*')
	for in_file in all_files:
		model=in_file.split('/')[-1].split('_')[2]
		rcp=in_file.split('/')[-1].split('_')[3]
		print(rcp,model,in_file)
		COU.zoom_data(in_file,var_name=old_varname,given_var_name=var,tags={'source':'CORDEX_BC','model':model,'rcp':rcp})

for var,old_varname in zip(['tas','pr','TXx','RX1','RX5'],['tas','pr','tasmax','pr','pr']):
	COU.zoom_data('/p/projects/tumble/carls/shared_folder/data/EWEMBI/mon_year/mon_'+var+'_EWEMBI_1979-2014.nc4',var_name=old_varname,given_var_name=var,tags={'source':'EWEMBI','model':'EWEMBI','rcp':'historical'})



COU.meta_gather_information()

COU.meta_load()

COU.griddedData_read()




COU.griddedData_print()

COU.calculate_areaAverage()



#
